<?php
/**
 * Plugin Name: Programming Project Management
 * Plugin URI: https://www.unesma.net
 * Description: Management of programming projects.
 * Version: 1.0
 * Author: Luciano Nahuel Vitale
 * Author URI: https://luvitale.net
 */

function create_projects_db() {
  global $wpdb;
  $project_table = $wpdb->prefix . "projects";

  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

  $created = dbDelta(
   "CREATE TABLE $project_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     name varchar(100) NOT NULL DEFAULT '',
     description varchar(256) DEFAULT NULL,
     repo varchar(100) NOT NULL DEFAULT '',
     url varchar(100) DEFAULT NULL,
     image mediumtext NOT NULL DEFAULT '',
     PRIMARY KEY (id),
     KEY name (name),
     KEY repo (repo)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );
}
register_activation_hook( __FILE__, 'create_projects_db' );

add_action( 'wp_enqueue_scripts', 'wpsp_enqueue_scripts' );
/*
* Enqueue our CSS to the front end
*
* @since 0.1
*/
function wpsp_enqueue_scripts() {
  wp_enqueue_style( 'wp-show-projects', plugins_url( "css/wp-show-projects.css", __FILE__ ) );
  wp_enqueue_style( 'wp-bootstrap-css', plugins_url( "vendor/twbs/bootstrap/dist/css/bootstrap.min.css", __FILE__ ) );
  wp_enqueue_script( 'wp-bootstrap-js', plugins_url( "vendor/twbs/bootstrap/dist/js/bootstrap.min.js", __FILE__ ) );
  wp_register_script( "wp-aos-js", plugins_url( "node_modules/aos/dist/aos.js", __FILE__ ) );
  wp_register_style( "wp-aos-css", plugins_url( "node_modules/aos/dist/aos.css", __FILE__ ) );
  wp_register_script( 'ppm-main', plugins_url( "js/main.js", __FILE__ ) );
}

function shortcode_show_projects() {
	global $wpdb;
	$project_table = $wpdb->prefix . 'projects';
	$projects = $wpdb->get_results("SELECT * FROM $project_table");
  wp_enqueue_style('wp-aos-css');
  wp_enqueue_script('wp-aos-js');
  wp_enqueue_script('ppm-main');
  ?>
  <?php ob_start(); ?>
  <div class="album py-5 projects">
    <div class="container">
      <div class="row">
      <?php foreach($projects as $project):
        $args = array(
         'repo' => $project->repo,
         'url' => $project->url,
         'name' => $project->name,
         'image' => $project->image
        );
      ?>
        <div class="col-md-4" data-aos="fade-right">
          <div class="project card mb-4 box-shadow">
            <a href="<?=$args['repo']?>" class="project-link">
              <img class="project-img-top card-img-top" style="width:100%; height: 200px; background-size: cover;" src="data:image/png;base64,<?php echo $args['image'] ?>" alt="Card image cap">
            </a>
            <div class="card-body project-body">
              <a href="<?=$args['repo']?>">
                <h2 class="card-title project-title">
                  <span class="code">&lt; </span>
                    <?=$args['name']?>
                  <span class="code"> &gt;</span>
                </h2>
              </a>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="<?=$args['repo']?>" class="btn btn-sm btn-outline-secondary btn-project">Repo</a>
                  <?php if ($args['url']):  ?>
                    <a href="<?=$args['url']?>" class="btn btn-sm btn-outline-secondary btn-project">URL</a>
                  <?php endif ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach ?>
      </div>
    </div>
  </div>
  <?php $output = ob_get_clean(); ?>
  <?php return $output;
}
add_shortcode('projects', 'shortcode_show_projects');
