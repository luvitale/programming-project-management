# Programming Project Management
Wordpress plugin that create a project database with these columns:
* id	bigint(20) unsigned AI
* name	varchar(100) []
* description	varchar(256) NULL
* repo	varchar(100) []
* url	varchar(100) NULL
* image	mediumtext

## Shortcodes

### [projects]
Show all projects with style in css/wp-show-projects.css stylesheet.
